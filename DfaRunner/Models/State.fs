module Models.State

open Extensions

type State = private State of string

module State =
    let private isInvalidName (s: string) =
        String.isEmpty s
        || String.exists (fun c -> c = ';' || c = ':' || c = ',') s

    let create (s: string) =
        if isInvalidName s
        then None
        else Some(State s)

    let name (State n) = n