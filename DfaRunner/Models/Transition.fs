module Models.Transition

open Models.State

type Transition = Transition of State * char * State

module Transition =
    let current (Transition (current, _, _)) = current
    let input (Transition (_, input, _)) = input
    let next (Transition (_, _, next)) = next