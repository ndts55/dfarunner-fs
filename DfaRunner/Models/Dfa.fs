module Models.Dfa

open Models.State
open Extensions
open Models.Transition

type Dfa = private Dfa of char Set * State Set * State * Transition Set * State Set

module Dfa =
    let private validate s q q0 d a =
        let slist = s |> Set.toList
        let allEqualSigma = List.exists ((<>) slist) >> not
        let withState state = Set.filter (Transition.current >> (=) state)
        let toInputs state = d
                            |> withState state
                            |> Set.toList
                            |> List.map Transition.input
        let allInQ ss = Set.isSubset ss q
        not (Set.isEmpty q)
        && Set.contains q0 q
        && allInQ a
        && d |> Set.map Transition.current |> allInQ
        && d |> Set.map Transition.next |> allInQ
        && q |> Set.toList |> List.map toInputs |> allEqualSigma

    // A = (Σ, Q, q0 , δ, A)
    let create s q q0 d a =
        if validate s q q0 d a
        then Some(Dfa(s, q, q0, d, a))
        else None

    let private parseInit q q0 s =
        let parse (s: string) =
            let parts = String.split [ ':' ] s
            if Array.length parts <> 2
            then Error("not enough parts in " + s)
            else
                match State.create parts.[1] with
                | None -> Error("invalid identifier in " + s)
                | Some q -> Ok q
        match (q0, parse s) with
            | (Some _, _) -> Error "initial cannot be defined more than once"
            | (_, Ok state) ->
                Ok((Set.add state q), Some state)
            | (_, Error s) -> Error s

    let private parseAccept q a s =
        let parse (s: string) =
            let parts = String.split [ ':' ] s
            if Array.length parts <> 2
            then Error("not enough parts in" + s)
            else parts.[1]
                |> String.split [ ',' ]
                |> Array.map State.create
                |> Array.fold (fun acc so ->
                    match (acc, so) with
                    | (Error s, _) -> Error s
                    | (_, None) -> Error("invalid identifier in " + s)
                    | (Ok ss, Some state) -> Ok(state :: ss)) (Ok [])
        match parse s with
        | Error s -> Error s
        | Ok states -> Ok((Set.addAll states q), (Set.addAll states a))

    let private parseTransition sigma q delta s =
        let parse (s: string) =
            let parts = String.split [ ',' ] s
            if Array.length parts <> 3
            then Error("not enough parts in " + s)
            else
                let c = State.create parts.[0]
                let i = Char.parse parts.[1]
                let n = State.create parts.[2]
                match (c, i, n) with
                | (Some c, Some i, Some n) -> Ok(Transition(c, i, n))
                | (_, None, _) -> Error("invalid input in " + s)
                | _ -> Error("invalid identifier in " + s)
        match parse s with
        | Error s -> Error s
        | Ok(Transition(c, i, n)) ->
            Ok(Set.add i sigma,
                Set.addAll [ c; n ] q,
                Set.add (Transition(c, i, n)) delta)

    let from (s: string) = // TODO test and possibly move from + parse functions
        let mutable sigma = Set.empty
        let mutable q = Set.empty
        let mutable q0 = None
        let mutable delta = Set.empty
        let mutable a = Set.empty
        let parse (s: string) =
            let s = String.removeWhiteSpace s
            if s.StartsWith("init:")
            then
                match parseInit q q0 s with
                | Error s -> Error s
                | Ok(nq, nq0) ->
                    q <- nq
                    q0 <- nq0
                    Ok()
            elif s.StartsWith("accept:")
            then
                match parseAccept q a s with
                | Error s -> Error s
                | Ok(nq, na) ->
                    q <- nq
                    a <- na
                    Ok()
            else
                match parseTransition sigma q delta s with
                | Error s -> Error s
                | Ok(nsigma, nq, ndelta) ->
                    sigma <- nsigma
                    q <- nq
                    delta <- ndelta
                    Ok()
        let addResults r1 r2 =
            match (r1, r2) with
            | (Error s, Error m) -> Error (s + "\n" + m)
            | (Error s, _) | (_, Error s) -> Error s
            | _ -> Ok ()
        let processSource =
            String.split [';';'\n']
            >> Array.filter (String.isEmpty >> not)
            >> Array.map parse
            >> Array.reduce addResults
        
        let result = processSource s
        
        match (q0, result) with
        | (None, Error s) -> Error (s + "\ninitial has not been defined")
        | (None, _) -> Error "initial has not been defined"
        | (_, Error s) -> Error s
        | (Some q0, Ok ()) ->
            match create sigma q q0 delta a with
            | None -> Error ("dfa is invalid " + s)
            | Some dfa -> Ok dfa

    let accepts (Dfa (sigma, q, q0, delta, a)) (word: string) =
        let cs = word.ToCharArray()
        let delta = Set.toList delta
        let transitionFor s k (Transition (c, i, _)) = s = c && k = i
        let rec finalState (rest: string) current =
            match String.splitFirst rest with
            | None -> current
            | Some (c, r) ->
                List.find (transitionFor current c) delta
                |> Transition.next
                |> finalState r
        if Array.exists (fun c -> Set.contains c sigma |> not) cs
        then false
        else Set.contains (finalState word q0) a