module DfaRunner.Tests.ExtensionsTests

open Xunit
open FsUnit.Xunit
open Extensions

[<Fact>]
let ``empty string is empty``() =
    ""
    |> String.isEmpty
    |> should be True

[<Fact>]
let ``non-empty string is not empty``() =
    "not empty"
    |> String.isEmpty
    |> should be False

[<Fact>]
let ``removeWhiteSpace removes all whitespace``() =
    "    st \n ri  \r\n n   \t g"
    |> String.removeWhiteSpace
    |> should equal "string"

[<Fact>]
let ``split splits``() =
    "str#ing"
    |> String.split [ '#' ]
    |> should equal [| "str"; "ing" |]

[<Theory>]
[<InlineData("")>]
[<InlineData("ab")>]
let ``parse returns none when string not exactly one char``(s: string) =
    s
    |> Char.parse
    |> should equal None

[<Fact>]
let ``parse returns some when string one char`` () =
    "a"
    |> Char.parse
    |> Option.isSome
    |> should be True
