module DfaRunner.Tests.DfaTests

open Models.State
open Models.Transition
open Models.Dfa
open Xunit
open FsUnit.Xunit

let sigma = set [ 'a'; 'b' ]
let q = set [
    State.create "q0" |> Option.get;
    State.create "q1" |> Option.get
 ]
let q0 = q |> Set.toList |> List.head
let q1 = State.create "q1" |> Option.get
let delta =
    let a = Set.toArray q
    set [
    Transition(a.[0], 'a', a.[1]);
    Transition(a.[0], 'b', a.[0]);
    Transition(a.[1], 'a', a.[1]);
    Transition(a.[1], 'b', a.[0])
 ]

let a = q |> Set.toList |> List.tail |> set

[<Fact>]
let ``dfa with no states is invalid``() =
    Dfa.create sigma (Set.empty) q0 delta a
    |> should equal None

[<Fact>]
let ``dfa where q0 is not in q is invalid``() =
    Dfa.create sigma q (State.create "q2" |> Option.get) delta a
    |> should equal None

[<Fact>]
let ``dfa where a contains states which are not in q is invalid``() =
    Dfa.create sigma q q0 delta (set [ State.create "q2" |> Option.get ])
    |> should equal None

[<Fact>]
let ``dfa where transitions are missing for states is invalid``() =
    let d = set [
        Transition(q0, 'a', q0);
        Transition(q0, 'b', q1);
        Transition(q1, 'a', q0)
    ]
    Dfa.create sigma q q0 d a
    |> should equal None

[<Fact>]
let ``dfa where transitions are defined more than once is invalid``() =
    let d = set [
        Transition(q0, 'a', q0);
        Transition(q0, 'a', q1);
        Transition(q0, 'b', q1);
        Transition(q1, 'a', q0)
    ]
    Dfa.create sigma q q0 d a
    |> should equal None

[<Fact>]
let ``dfa where current states are not in q is invalid``() =
    let d = set [
        Transition(q0, 'a', q0);
        Transition(q0, 'b', q1);
        Transition(q1, 'a', q0);
        Transition(q1, 'b', q1);
        Transition(State.create "q2" |> Option.get, 'a', q0)
    ]
    Dfa.create sigma q q0 d a
    |> should equal None

[<Fact>]
let ``dfa where next states are not in q is invalid``() =
    let d = set [
        Transition(q0, 'b', q1);
        Transition(q1, 'a', q0);
        Transition(q1, 'b', q1);
        Transition(q0, 'a', State.create "q2" |> Option.get)
    ]
    Dfa.create sigma q q0 d a
    |> should equal None

[<Fact>]
let ``simple dfa is valid``() =
    Dfa.create sigma q q0 delta a
    |> Option.isSome
    |> should be True

[<Fact>]
let ``dfa does not accept words with symbol outside of sigma``() =
    let dfa = Dfa.create sigma q q0 delta a |> Option.get
    Dfa.accepts dfa "czcz"
    |> should be False

[<Theory>]
[<InlineData("b")>]
[<InlineData("ab")>]
let ``dfa does not accept words outside of its language`` (word: string) =
    let dfa = Dfa.create sigma q q0 delta a |> Option.get
    Dfa.accepts dfa word
    |> should be False

[<Theory>]
[<InlineData("aba")>]
[<InlineData("aaabbbaaa")>]
let ``dfa does accept words inside its language`` (word: string) =
    let dfa = Dfa.create sigma q q0 delta a |> Option.get
    Dfa.accepts dfa word
    |> should be True
