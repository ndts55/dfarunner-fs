module DfaRunner.Tests.StateTests

open FsUnit.Xunit
open Xunit
open Models.State

[<Fact>]
let ``State can't have empty name``() =
    State.create ""
    |> should equal None

[<Theory>]
[<InlineData("q;0")>]
[<InlineData("q:0")>]
[<InlineData("q,0")>]
let ``State can't have name with ; : or ,`` (name: string) =
    name
    |> State.create
    |> should equal None


[<Theory>]
[<InlineData("q")>]
[<InlineData("q0")>]
[<InlineData("state_1-one!@#$%^&*()_+{}[]«»'")>]
let ``State is some`` (name: string) =
    name
    |> State.create
    |> Option.isSome
    |> should be True

[<Fact>]
let ``State.name gets states name``() =
    State.create "q0"
    |> Option.get
    |> State.name
    |> should equal "q0"
