module DfaRunner.Tests.TransitionTests

open Models.State
open Models.Transition
open Xunit
open FsUnit.Xunit

let q0 = State.create "q0" |> Option.get
let q1 = State.create "q1" |> Option.get
let input = 'a'
let transition = Transition(q0, input, q1)

[<Fact>]
let ``Transition.current gets current state``() =
    transition
    |> Transition.current
    |> should equal q0

[<Fact>]
let ``Transition.input gets input``() =
    transition
    |> Transition.input
    |> should equal input

[<Fact>]
let ``Transition.next gets next state``() =
    transition
    |> Transition.next
    |> should equal q1
