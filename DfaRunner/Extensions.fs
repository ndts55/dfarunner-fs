module Extensions

open System

module String =
    let isEmpty (s: string) =
        String.length s = 0
    let removeWhiteSpace =
        String.filter (Char.IsWhiteSpace >> not)
    let split seps (s: string) =
        s.Split(seps |> List.toArray)
    let splitFirst (s: string) =
        if String.length s < 1 then None
        else Some (s.Substring(0,1) |> Char.Parse, s.Substring(1))
        
module Char =
    let parse s =
        if String.length s <> 1
        then None
        else Some (Char.Parse s)
        
module Set =
    let addAll r s =
        let mutable s = s
        Set.iter (fun e -> s <- Set.add e s) s
        s